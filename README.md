https://www.odesk.com/jobs/MySQL-SQL-development_%7E01fbff3b1fcef9ad2f



There are 4 tables in a MySQL database attached.  There is a query attached that takes over 2 minutes to run, which is too long.  I want you to rewrite this query, and fix the indexes, so that it is 100% perfectly efficient.  Maybe the answer is to remove the derived tables and remove the UNION?

I cannot provide a dump of the database.  You must invent your own test data to insert yourself into the tables. 

See file attached.

I need the answer fast.


Hi I have this much data:

customers - 3,000 rows
locationcodes - 40,000 rows
cardinventoryalerts - 11,000 rows
cardbatch - 9,000,000 rows