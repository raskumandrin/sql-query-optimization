Select
	cia.cia_orderamount
	,cia.cia_notes
	,cia.cia_mincount
	,cia.cia_customerid
	,cia.cia_id
	,cia.cia_locationid
	,lc.locationcode
	,T1.instock
	,c.id as customerid
	,c.customer
From cardinventoryalerts cia
Inner join customers c on cia.cia_customerid = c.id and c.useautocardorder = 1
Inner Join locationcodes lc on cia.cia_locationid = lc.id
left JOIN (
	SELECT
		cb.customer
		,Case when cb.locationcode is null or cb.locationcode = '' Then NULL Else cb.locationcode End as locationcode
		,sum( CASE WHEN cb.issued = 'no' THEN 1 ELSE 0 END) as instock
	FROM cardbatch cb
	Group By cb.customer, cb.locationcode
	)
	as T1 on lc.locationcode = T1.locationcode and T1.customer = c.customer
WHERE
	cia_mincount > T1.instock
	or T1.instock is NULL
	
UNION

Select
	cia.cia_orderamount
	,cia.cia_notes
	,cia.cia_mincount
	,cia.cia_customerid
	,cia.cia_id
	,-1
	,NULL
	,T1.instock
	,c.id as customerid
	,c.customer
From cardinventoryalerts cia
Inner join customers c on cia.cia_customerid = c.id and c.useautocardorder = 1
left JOIN (
	SELECT
		cb.customer
		,-1
		,sum( CASE WHEN cb.issued = 'no' THEN 1 ELSE 0 END) as instock
	FROM cardbatch cb
	where cb.locationcode is null or cb.locationcode = ''
	Group By cb.customer
  	)
	as T1 on c.customer = T1.customer
WHERE
	(cia_mincount > T1.instock or T1.instock is NULL)
	and cia.cia_locationid is null
