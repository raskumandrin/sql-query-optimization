CREATE TABLE `cardinventoryalerts` (
`cia_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`cia_customerid` int(11) unsigned NOT NULL,
`cia_locationid` int(11) unsigned DEFAULT NULL,
`cia_orderamount` int(11) DEFAULT NULL,
`cia_mincount` int(11) DEFAULT NULL,
`cia_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
`cia_createdby` varchar(50) DEFAULT NULL,
`cia_notes` varchar(500) DEFAULT NULL,
PRIMARY KEY (`cia_id`),
KEY `fk_ciacustid` (`cia_customerid`),
KEY `fk_cialocationid` (`cia_locationid`)
) ENGINE=InnoDB AUTO_INCREMENT=255782 DEFAULT CHARSET=latin1;

CREATE TABLE `locationcodes` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`customer` varchar(100) DEFAULT NULL,
`locationcode` varchar(50) DEFAULT NULL,
`parentid` int(11) DEFAULT NULL,
`alias` varchar(100) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `id` (`id`),
KEY `idx_parent` (`parentid`),
KEY `idx_customer` (`customer`),
KEY `idx_loccode` (`locationcode`)
) ENGINE=InnoDB AUTO_INCREMENT=41988 DEFAULT CHARSET=latin1;

CREATE TABLE `cardbatch` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`cardnumber` varchar(100) DEFAULT NULL,
`issued` varchar(10) DEFAULT 'no',
`firstname` varchar(100) DEFAULT NULL,
`lastname` varchar(100) DEFAULT NULL,
`customer` varchar(100) DEFAULT NULL,
`locationcode` varchar(50) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `id` (`id`),
UNIQUE KEY `cardnumber` (`cardnumber`),
KEY `customer_loc` (`customer`,`locationcode`),
KEY `customer_loc_issued` (`customer`,`locationcode`,`issued`),
KEY `customer` (`customer`),
KEY `cardnumber_customer` (`cardnumber`,`customer`)
) ENGINE=InnoDB AUTO_INCREMENT=8854518 DEFAULT CHARSET=latin1;

CREATE TABLE `customers` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`customer` varchar(100) DEFAULT NULL,
`address` varchar(10) NOT NULL DEFAULT 'yes',
`balance` varchar(20) DEFAULT '0',
`useautocardorder` int(11) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `id` (`id`),
UNIQUE KEY `customer` (`customer`)
) ENGINE=InnoDB AUTO_INCREMENT=2779 DEFAULT CHARSET=latin1;


Select cia.cia_orderamount, cia.cia_notes, cia.cia_mincount, cia.cia_customerid, cia.cia_id, cia.cia_locationid, lc.locationcode, T1.instock, c.id as customerid, c.customer
From cardinventoryalerts cia
Inner join customers c on cia.cia_customerid = c.id and c.useautocardorder = 1
Inner Join locationcodes lc on cia.cia_locationid = lc.id
left JOIN 
  (SELECT cb.customer, Case when cb.locationcode is null or cb.locationcode = '' Then NULL Else cb.locationcode End as locationcode,
    sum( CASE WHEN cb.issued = 'no' THEN 1 ELSE 0 END) as instock
  FROM cardbatch cb
  Group By cb.customer, cb.locationcode
  ) as T1 on lc.locationcode = T1.locationcode and T1.customer = c.customer
WHERE (cia_mincount > T1.instock or T1.instock is NULL)
UNION
Select cia.cia_orderamount, cia.cia_notes, cia.cia_mincount, cia.cia_customerid, cia.cia_id, -1, NULL, T1.instock, c.id as customerid, c.customer
From cardinventoryalerts cia
Inner join customers c on cia.cia_customerid = c.id and c.useautocardorder = 1
left JOIN 
  (SELECT cb.customer, -1, sum( CASE WHEN cb.issued = 'no' THEN 1 ELSE 0 END) as instock
  FROM cardbatch cb
  where cb.locationcode is null or cb.locationcode = ''
  Group By cb.customer
  ) as T1 on c.customer = T1.customer
WHERE (cia_mincount > T1.instock or T1.instock is NULL) and cia.cia_locationid is null
